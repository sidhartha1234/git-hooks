# Path to UI directory
DIR := $(shell pwd)
# Path to frontend directory
UI_DIR := $(DIR)/hybris

test_css:
	./scripts/lint-css.sh

test_js:
	./scripts/lint-js.sh

test_assets:
	make test_css
	make test_js

install:
	npm install
	cd $(UI_DIR) && npm install
	cd $(DIR)

clean:
	@rm -rf node_modules
