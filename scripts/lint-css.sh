#!/bin/bash
cd "$(git rev-parse --show-toplevel)"
STYLELINT="stylelint"
pwd

if [[ ! "$STYLELINT" ]]; then
  printf "\t\033[41mPlease install ESlint\033[0m (npm install eslint) from root \n"
  exit 1
fi

# Check for staged JS or JSX files
STAGED_FILES=($(git diff --cached --name-only --diff-filter=ACM | grep ".css\{0,1\}$"))

echo "ESLint'ing ${#STAGED_FILES[@]} files"

if [[ "$STAGED_FILES" = "" ]]; then
  exit 0
fi

$STYLELINT "${STAGED_FILES[@]}" --cache

STYLELINT_EXIT="$?"

# Re-add files since they may have been fixed
git add "${STAGED_FILES[@]}"

if [[ "${STYLELINT_EXIT}" == 0 ]]; then
  printf "\n\033[42mCOMMIT SUCCEEDED\033[0m\n"
else
  printf "\n\033[41mCOMMIT FAILED:\033[0m Fix eslint errors and try again\n"
  exit 1
fi

exit $?
